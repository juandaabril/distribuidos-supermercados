package com.distribuidos.supermercados.caja;

import java.util.Queue;

import com.distribuidos.supermercados.commun.model.Mercado;

/**
 * Clase ProcesadorCaja. Clase para procesar los mercados que se encuentran en
 * la cola de la caja.
 * 
 * @author Paola
 * @author Juan David
 */
public class ProcesadorCaja extends Thread {

	private Queue<Mercado> cola;
	private long sleepTime;
	private long cajaTime;

	public ProcesadorCaja(Queue<Mercado> cola, long sleepTime, long cajaTime) {
		this.cola = cola;
		this.sleepTime = sleepTime;
		this.cajaTime = cajaTime;
	}

	public void run() {
		while (true) {
			try {
				/****************************************************
				 * Si la cola no esta vacia, se procesa los mercados
				 *****************************************************/
				if (!cola.isEmpty()) {
					Mercado mercado = cola.poll();
					if (mercado != null) {
						System.out.println("Procesado Mercado:" + mercado);
						Thread.sleep((long) (1000 * Math.random() + cajaTime));
					}

				}

				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
