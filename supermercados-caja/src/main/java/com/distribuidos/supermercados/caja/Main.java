package com.distribuidos.supermercados.caja;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Properties;

import com.distribuidos.supermercados.commun.interfaces.Caja;
import com.distribuidos.supermercados.commun.interfaces.MonitorCaja;
import com.distribuidos.supermercados.commun.interfaces.SuperMercadoRegistry;
import com.distribuidos.supermercados.commun.util.Utils;

/**
 * Clase Main. Clase principal encargada de obtener los datos de las propiedades
 * y registrar la caja en el monitor.
 * 
 * @author Paola
 * @author Juan David
 */
public class Main {

	public static void main(String args[]) throws FileNotFoundException,
			IOException, NotBoundException {

		System.out.println("Proyecto - SUPERMERCADOS - Sistemas Distribuidos - Caja");
		Utils.setCodeBase(SuperMercadoRegistry.class);

		/*************************************************************
		 * Se obtiene los datos de conexion del archivo de propiedades
		 *************************************************************/
		Properties prop = new Properties();
		prop.load(new FileInputStream("caja.properties"));
		String ipRegistry = prop.getProperty("ip");
		int puertoRegistry = Integer.valueOf(prop.getProperty("puerto"));
		long sleepTime = Long.valueOf(prop.getProperty("sleepTime"));
		long cajaTime = Long.valueOf(prop.getProperty("cajaTime"));
		
		/*****************************
		 * Se crea la caja
		 *****************************/
		Caja caja = new CajaImpl(sleepTime, cajaTime);

		/*********************************************************
		 * Se crea la caja utilizando el registro del supermercado
		 *********************************************************/
		final Registry registry = LocateRegistry.getRegistry(ipRegistry, puertoRegistry);
		final MonitorCaja monitor = (MonitorCaja) registry.lookup(MonitorCaja.RMI_NAME);
		monitor.addCaja(caja);

		System.out.println("Caja Registrada con exito.  Presione enter para terminar");

		System.in.read();

	}

}
