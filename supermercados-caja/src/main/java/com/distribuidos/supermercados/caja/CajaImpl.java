package com.distribuidos.supermercados.caja;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.Queue;

import com.distribuidos.supermercados.commun.interfaces.Caja;
import com.distribuidos.supermercados.commun.model.Mercado;

/**
 * Clase CajaImpl. Clase que contiene la lógica necesaria para implementar la
 * interface Caja.
 * 
 * @author Paola
 * @author Juan David
 */
public class CajaImpl extends UnicastRemoteObject implements Caja {

	private static final long serialVersionUID = 6701232875753468976L;
	private String nombre;
	private boolean activo = false;
	private Queue<Mercado> cola;

	public CajaImpl(long sleepTime, long cajaTime) throws RemoteException {
		super();
		this.cola = new LinkedList<Mercado>();
		
		/**********************************************************************
		 * Se crea un hilo para realizar el procesamiento de la cola de la caja
		 *********************************************************************/
		ProcesadorCaja procesadorCaja = new ProcesadorCaja(cola, sleepTime,	cajaTime);
		procesadorCaja.start();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.supermercados.commun.interfaces.Caja#
	 * setNombre(java.lang.String)
	 */
	@Override
	public void setNombre(String nombre) throws RemoteException {
		this.nombre = nombre;
		System.out.println("(Caja)(setNombre):" + nombre);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.supermercados.commun.interfaces.Caja# getNombre()
	 */
	@Override
	public String getNombre() throws RemoteException {
		System.out.println("(Caja)(getNombre):" + nombre);
		return this.nombre;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.supermercados.commun.interfaces.Caja#
	 * addMercado(com.distribuidos.supermercados.commun.model.Mercado)
	 */
	@Override
	public synchronized void addMercado(Mercado mercado) throws RemoteException {
		this.cola.add(mercado);
		System.out.println("(Caja)(addMercado):" + mercado);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.supermercados.commun.interfaces.Caja# isActivo()
	 */
	@Override
	public boolean isActivo() {
		System.out.println("(Caja)(isActivo):" + activo);
		return activo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.supermercados.commun.interfaces.Caja#
	 * setActivo(boolean)
	 */
	@Override
	public void setActivo(boolean activo) {
		this.activo = activo;
		System.out.println("(Caja)(setActivo):" + activo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.distribuidos.supermercados.commun.interfaces.Caja#
	 * getCantidadElementosCola()
	 */
	@Override
	public int getCantidadElementosCola() throws RemoteException {
		return this.cola.size();
	}
}
