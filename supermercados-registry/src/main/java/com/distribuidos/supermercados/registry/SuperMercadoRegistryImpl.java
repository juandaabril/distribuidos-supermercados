package com.distribuidos.supermercados.registry;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import com.distribuidos.supermercados.commun.interfaces.Inventario;
import com.distribuidos.supermercados.commun.interfaces.MonitorCaja;
import com.distribuidos.supermercados.commun.interfaces.SuperMercadoRegistry;

/**
 * Clase SuperMercadoRegistryImpl. Clase que contiene la lógica necesaria para implementar la
 * interface SuperMercadoRegistry.
 * 
 * @author Paola
 * @author Juan David
 */
public class SuperMercadoRegistryImpl extends UnicastRemoteObject implements SuperMercadoRegistry {

	private static final long serialVersionUID = -8127278608590469932L;

	/** Referencia al registry */
	private Registry registry;

	public SuperMercadoRegistryImpl(Registry registry) throws RemoteException {
		super();
		this.registry = registry;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.distribuidos.supermercados.commun.interfaces.SuperMercadoRegistry
	 * #registrar(com.distribuidos.supermercados.commun.interfaces.MonitorCaja)
	 */
	public synchronized void registrar(MonitorCaja monitor) throws RemoteException {
		try {
			registry.rebind(MonitorCaja.RMI_NAME, monitor);
			System.out.println("(SuperMercadoRegistry)(registrar):" + MonitorCaja.RMI_NAME);
		} catch (RemoteException e) {
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.distribuidos.supermercados.commun.interfaces.SuperMercadoRegistry
	 * #registrar(com.distribuidos.supermercados.commun.interfaces.Inventario)
	 */
	public synchronized void registrar(Inventario inventario) throws RemoteException {
		try {
			registry.rebind(Inventario.RMI_NAME, inventario);
			System.out.println("(SuperMercadoRegistry)(registrar):" + Inventario.RMI_NAME);
		} catch (RemoteException e) {
			e.printStackTrace();
			throw e;
		}
	}

}
