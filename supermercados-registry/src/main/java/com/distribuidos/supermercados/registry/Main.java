package com.distribuidos.supermercados.registry;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Properties;

import com.distribuidos.supermercados.commun.interfaces.SuperMercadoRegistry;
import com.distribuidos.supermercados.commun.util.Utils;

/**
 * Clase Main de la aplicación. Se instancia un objeto de tipo SuperMercadosRegistry el
 * cual contiene la lógica para añadir objetos remotos al registry
 * 
 * @author Paola
 * @author Juan David
 */
public class Main {

	public static void main(String args[]) throws FileNotFoundException,
			IOException {
		
		System.out.println("Proyecto - SUPERMERCADOS - Sistemas Distribuidos");
		
		/*************************************************************
		 * Se obtiene los datos de conexion del archivo de propiedades
		 *************************************************************/
		Properties prop = new Properties();
		prop.load(new FileInputStream("registry.properties"));
		int puertoRegistry = Integer.valueOf(prop.getProperty("puerto"));
		String host = prop.getProperty("hostname");
		
		/***************************************************
		 * Se realizan set a la maquina virtual
		 **************************************************/
		Utils.setCodeBase(SuperMercadoRegistry.class);
		Utils.setHostName(host);
		
		/***************************************************
		 * Obtenemos el registry que debe estar ejecutandose
		 **************************************************/
		final Registry registry = LocateRegistry.getRegistry(puertoRegistry);
		SuperMercadoRegistry superMercadoRegistry = new SuperMercadoRegistryImpl(registry);
		registry.rebind(SuperMercadoRegistry.RMI_NAME, superMercadoRegistry);
		
		System.out.println("SuperMercadoRegistry ejecutado con extio.  Presione enter para terminar");
		
		System.in.read();
		
	}
}
