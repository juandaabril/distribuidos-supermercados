package com.distribuidos.supermercados.mercados;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.distribuidos.supermercados.commun.interfaces.SuperMercadoRegistry;
import com.distribuidos.supermercados.commun.util.Utils;

/**
 * Clase Main. Clase principal encargada de obtener los datos de las propiedades
 * e inicializar la generación de mercados
 * 
 * @author Paola
 * @author Juan David
 */
public class Main {

	public static void main(String args[]) throws FileNotFoundException,
			IOException, NotBoundException, InterruptedException {

		System.out.println("Proyecto - SUPERMERCADOS - Sistemas Distribuidos - Generador Mercados");
		Utils.setCodeBase(SuperMercadoRegistry.class);

		/*************************************************************
		 * Se obtiene los datos de conexion del archivo de propiedades
		 *************************************************************/
		Properties prop = new Properties();
		prop.load(new FileInputStream("mercados.properties"));
		String ipRegistry = prop.getProperty("ip");
		int puertoRegistry = Integer.valueOf(prop.getProperty("puerto"));
		int numeroHilos = Integer.valueOf(prop.getProperty("hilos"));
		int iteraciones = Integer.valueOf(prop.getProperty("iteraciones"));
		long sleepTime = Long.valueOf(prop.getProperty("sleepTime"));
		
		List<GeneradorMercados> lista = new ArrayList<GeneradorMercados>();
		for(int i = 0; i< iteraciones; i++){
			for(int j = 0; j< numeroHilos; j++){
				GeneradorMercados mercado = new GeneradorMercados(ipRegistry, puertoRegistry);
				lista.add(mercado);
				mercado.start();
			}
			Thread.sleep(sleepTime);
		}
		
		
		for(GeneradorMercados mercados : lista){
			mercados.join();
		}
		
		
		System.out.println("Se generaron "  +  (iteraciones*numeroHilos) + " .  Presione enter para terminar");

		System.in.read();

	}

}
