package com.distribuidos.supermercados.mercados;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.distribuidos.supermercados.commun.interfaces.Caja;
import com.distribuidos.supermercados.commun.interfaces.Inventario;
import com.distribuidos.supermercados.commun.interfaces.MonitorCaja;
import com.distribuidos.supermercados.commun.model.Mercado;

/**
 * Clase GeneradorMercados. Clase encargada de generar el mercado. Solicita al
 * inventario un mercado, solicita al monitor en que caja se debe ubicar el
 * mercado.
 * 
 * @author Paola
 * @author Juan David
 */
public class GeneradorMercados extends Thread {

	private String ipRegistry;
	private int puertoRegistry;
	private long sleepTime = 500;

	public GeneradorMercados(String ipRegistry, int puertoRegistry) {
		this.ipRegistry = ipRegistry;
		this.puertoRegistry = puertoRegistry;
	}

	public void run() {
		try {

			/*****************************************
			 * Se busca en el registry el inventario
			 *****************************************/
			final Registry registry = LocateRegistry.getRegistry(ipRegistry, puertoRegistry);
			final Inventario inventario = (Inventario) registry.lookup(Inventario.RMI_NAME);
			final MonitorCaja monitorCaja = (MonitorCaja) registry.lookup(MonitorCaja.RMI_NAME);

			/**********************************************
			 * Se genera el mercador con base al inventario
			 **********************************************/
			Mercado mercado = inventario.generarMercado();

			if (mercado == null) {
				System.out.println("No hay mas inventario/mercados disponible");
				return;
			}

			/**********************************************
			 * Se busca en que caja ubicar el monitor
			 **********************************************/
			Caja caja = null;
			while (caja == null) {
				caja = monitorCaja.ubicarMercado();
				Thread.sleep(sleepTime);
			}

			/**********************************************
			 * Se añade el mercado a la caja
			 **********************************************/
			caja.addMercado(mercado);

			/**********************************************
			 * Proceso finalizado con exito
			 **********************************************/
			System.out.println("Mercado añadido a la cola con exito");

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
