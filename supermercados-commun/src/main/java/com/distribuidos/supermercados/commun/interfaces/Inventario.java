package com.distribuidos.supermercados.commun.interfaces;

import java.rmi.Remote;

import com.distribuidos.supermercados.commun.model.Mercado;

/**
 * Clase Inventario. Interface que expone los métodos necesarios para la
 * administración del inventario
 * 
 * @author Paola
 * @author Juan David
 */
public interface Inventario extends Remote {
	
	/** Nombre RMI para el IVENTARIO en el registry*/
	public static final String RMI_NAME = "Inventario";
	
	/**
	 * Método para generar un mercado en el inventario.
	 * @return Mercado con la información
	 * @throws java.rmi.RemoteException
	 */
	public Mercado generarMercado() throws java.rmi.RemoteException;

}
