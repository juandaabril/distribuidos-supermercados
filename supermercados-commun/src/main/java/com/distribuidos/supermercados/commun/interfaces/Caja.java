package com.distribuidos.supermercados.commun.interfaces;

import java.rmi.Remote;

import com.distribuidos.supermercados.commun.model.Mercado;

/**
 * Clase Caja. Interface que expone los métodos necesarios para la
 * administración de una caja.
 * 
 * @author Paola
 * @author Juan David
 */
public interface Caja extends Remote {

	/**
	 * Método para obtener el nombre de una caja
	 * @return Nombre de la caja
	 * @throws java.rmi.RemoteException
	 */
	public String getNombre() throws java.rmi.RemoteException;


	/**
	 * Método para realizar actualizar el nombre de la caja
	 * @param nombre Nombre de la caja 
	 * @throws java.rmi.RemoteException
	 */
	public void setNombre(String nombre) throws java.rmi.RemoteException;

	/**
	 * Método para añadir un mercado a la cola de la caja
	 * @param mercado Objeto con la información de la caja
	 * @throws java.rmi.RemoteException
	 */
	public void addMercado(Mercado mercado) throws java.rmi.RemoteException;

	/**
	 * Método que indica si la caja se encuentra activa o inactiva
	 * @return TRUE si la caja se encuentra activa. FALSE si la caja esta inactiva.
	 * @throws java.rmi.RemoteException
	 */
	public boolean isActivo() throws java.rmi.RemoteException;

	/**
	 * Método para cambiar el estado de una caja.
	 * @param activo TRUE para que la caja se active. FALSE para que la caja se inactive.
	 * @throws java.rmi.RemoteException
	 */
	public void setActivo(boolean activo) throws java.rmi.RemoteException;

	/**
	 * Método para obtener la cantidad de elementos en la cola de la caja.
	 * @return Cantidad de elementos en cola
	 * @throws java.rmi.RemoteException
	 */
	public int getCantidadElementosCola() throws java.rmi.RemoteException;

}
