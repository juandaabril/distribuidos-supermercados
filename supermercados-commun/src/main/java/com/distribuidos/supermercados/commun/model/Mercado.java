package com.distribuidos.supermercados.commun.model;

import java.io.Serializable;
import java.util.List;

/**
 * Clase Item. Objeto que representa un mercado.
 * 
 * @author Paola
 * @author Juan David
 */
public class Mercado implements Serializable {

	private static final long serialVersionUID = 2825856432435622624L;
	
	private String nombre;
	
	private List<Item> lista;

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Mercado [nombre=" + nombre + ", valor = " + getValorTotal() + "]";
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the lista
	 */
	public List<Item> getLista() {
		return lista;
	}

	/**
	 * @param lista the lista to set
	 */
	public void setLista(List<Item> lista) {
		this.lista = lista;
	}

	
	public double getValorTotal(){
		double valor = 0;
		for(Item item: lista){
			valor += item.getValor() * item.getCantidad();
		}
		return valor;
	}

}
