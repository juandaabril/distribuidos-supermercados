package com.distribuidos.supermercados.commun.interfaces;

import java.rmi.Remote;

/**
 * Clase SuperMercadoRegistry. Interface que expone los métodos necesarios para la
 * administración del supermercados-registry.
 * 
 * @author Paola
 * @author Juan David
 */
public interface SuperMercadoRegistry extends Remote {
	
	/** Nombre RMI para el SUPERMERCADOS REGISTRY en el registry*/
	public static final String RMI_NAME = "SuperMercadoRegistry";

	/**
	 * Interface para registrar un monitor en el registry.
	 * @param monitor Objeto con la información del monitor
	 * @throws java.rmi.RemoteException
	 */
	public void registrar(MonitorCaja monitor) throws java.rmi.RemoteException;

	/**
	 * Interface para registrar el inventario en el registry.
	 * @param inventario Objeto con la información del registry
	 * @throws java.rmi.RemoteException
	 */
	public void registrar(Inventario inventario) throws java.rmi.RemoteException;

}
