package com.distribuidos.supermercados.commun.util;

/**
 * Clase Utils. Clase que cuenta con dos métodos para realizar el set de algunas
 * propiedades en la maquina virtual de RMI
 * 
 * @author Paola
 * @author Juan David
 */
public class Utils {

	public static final String CODE_BASE = "java.rmi.server.codebase";
	public static final String HOST_NAME = "java.rmi.server.hostname";

	/**
	 * Método para especificar las ubicaciones desde las que las clases que se
	 * publican (por ejemplo: clases stub, clases personalizadas que implementan
	 * el tipo de retorno declarado de una llamada a un método remoto, o
	 * interfaces utilizadas por una clase de proxy o código auxiliar).
	 * 
	 * @param c
	 */
	public static void setCodeBase(Class<?> c) {
		String ruta = c.getProtectionDomain().getCodeSource().getLocation().toString();
		String path = System.getProperty(CODE_BASE);

		if (path != null && !path.isEmpty()) {
			ruta = path + " " + ruta;
		}
		System.setProperty(CODE_BASE, ruta);
	}

	/**
	 * Método para especificar el host que debe ser asociado con el objeto
	 * remoto, con el fin de permitir a los clientes porder invocar métodos en
	 * el objeto remoto.
	 * 
	 * @param host
	 */
	public static void setHostName(String host) {
		System.setProperty("java.rmi.server.hostname", host);
	}
}
