package com.distribuidos.supermercados.commun.model;

import java.io.Serializable;

/**
 * Clase Item. Objeto que representa un ITEM del mercado.
 * 
 * @author Paola
 * @author Juan David
 */
public class Item implements Serializable {

	private static final long serialVersionUID = 791440329324707230L;

	private String codigo;
	private String nombre;
	private int cantidad;
	private double valor;
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Item [codigo=" + codigo + ", nombre=" + nombre + ", cantidad="
				+ cantidad + ", valor=" + valor + "]";
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad
	 *            the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the valor
	 */
	public double getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}
}
