package com.distribuidos.supermercados.commun.interfaces;

import java.rmi.Remote;

/**
 * Clase MonitorCaja. Interface que expone los métodos necesarios para la
 * administración del monitor de las cajas.
 * 
 * @author Paola
 * @author Juan David
 */
public interface MonitorCaja extends Remote {

	/** Nombre RMI para el MONITOR en el registry*/
	public static final String RMI_NAME = "MonitorCaja";
	
	/**
	 * Método para registrar una nueva caja en el monitor.
	 * @param caja Objeto con la información de la caja
	 * @throws java.rmi.RemoteException
	 */
	public  void addCaja(Caja caja) throws java.rmi.RemoteException;

	/**
	 * Método para seleccionar la caja con la cola mas vacia.
	 * @return Caja donde se debe ubicar el mercado
	 * @throws java.rmi.RemoteException
	 */
	public Caja ubicarMercado() throws java.rmi.RemoteException;
}
