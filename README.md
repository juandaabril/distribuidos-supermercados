# Proyectos de Sistemas Distribuidos: Simulación SuperMercados RMI (Invocación de Métodos Remotos)
Proyecto Simulación SuperMercados RMI para la materia de sistemas distribuidos.

## Autores

* Paola Andrea Betancurt
* Juan David Abril

## Diagrama de Componentes

![supermercados-componentes.png](https://bitbucket.org/repo/zngL6X/images/3784047154-supermercados-componentes.png)

* **SuperMercadoRegistry**: Componte que expone la interface para el registro de objetos remotos en el registry. Por medio de este componente se registran los objetos encargados del inventario y el monitoreo de cajas.
* **Inventario**: Componente encargado de generar el inventario y los mercados. Cuenta con un archivo de propiedades donde se pueden personalizar la cantidad de mercados y productos a generar.
* **MonitorCaja**: Componente donde se registran las cajas, es el encargado de activar o inactivar cajas según el estado de las colas, también se encarga de indicar la caja con la cola más pequeña para la asignación de un nuevo mercado.
* **Caja**: Componente encargado de procesar cada uno de los mercados, cuenta con una cola de tipo FIFO donde se van ubicando.
* **GeneradorMercados**: Componente encargado de solicitar mercados al inventario y ubicarlos en la caja que indique el monitor.

## Demostración
En el siguiente vídeo se puede apreciar la ejecución del programa en 3 computadores diferentes.

[Sistemas Distribuidos - RMI - SuperMercado](http://youtu.be/9risOLYUoYQ)

## Ejecución del programa

Requerimientos: Es necesario tener instalada como mínimo la versión de java 1.7( JRE 1.7.0).

![supermercados-carpetas.png](https://bitbucket.org/repo/zngL6X/images/1918674017-supermercados-carpetas.png)

###supermercados-registry:

**Instrucciones**

* Ejecute el archivo **registry.bat** el cual ejecutara el **registry** que se encuentra en el path de java.
* Edite el archivo **registry.properties** con la siguiente información:
```
#!properties
 puerto: Puerto en el cual se encuentra levantado el registry.
 hostname: Ip del servidor donde se está levantando SuperMercadoRegistry

```
* Ejecute el archivo **supermercados-registry.bat** el cual ejecutara el SuperMercadoRegistry permitiendo registrar objetos remotos en el registry.

![supermercados-registry2.png](https://bitbucket.org/repo/zngL6X/images/3526595225-supermercados-registry2.png)

**NOTA:** El **registy** y el **supermercados-registry** se deben ejecutar en el mismo servidor.


###supermercados-monitor:

**Instrucciones**

* Edite el archivo **monitor.properties** con la siguiente información:
```
#!properties
 ip=Ip donde se encuentra levantado el registry.
 puerto=Puerto en el cual se encuentra levantado el registry.
 hostname=Ip del servidor donde se está levantando supermercados-monitor.
 sleepTime=Tiempo en mili-segundos para indicarle al monitor cada cuánto va a verificar el estado de las cajas.
 mercadosParaActivar=Cantidad de mercados en cola para activar una nueva caja.
 mercadosParaInactivar=Cantidad de mercados en cola para inactivar una caja.
```
* Ejecute el archivo **supermercados-monitor.bat** el cual ejecutara el supermercados-monitor permitiendo registrar cajas y verificar su estado.

![supermercados-monitor.png](https://bitbucket.org/repo/zngL6X/images/1468098061-supermercados-monitor.png)

###supermercados-inventario:

**Instrucciones**

* Edite el archivo **inventario.properties** con la siguiente información:
```
#!properties
 ip=Ip donde se encuentra levantado el registry.
 puerto=Puerto en el cual se encuentra levantado el registry.
 hostname=Ip del servidor donde se está levantando supermercados-monitor.
 mercadosAGenerar=Cantidad de mercados disponibles en el inventario.
 cantidadMaxItemPorMercado=Cantidad máxima  de items por Mercado.
 cantidadMaxCompraItem=Cantidad máxima que se puede comprar por item. 
```
* Ejecute el archivo **supermercados-inventario.bat** el cual ejecutara el supermercados-inventario.

![supermercados-inventario.png](https://bitbucket.org/repo/zngL6X/images/2350875425-supermercados-inventario.png)

###supermercados-caja:

**Instrucciones**

* Edite el archivo **caja.properties** con la siguiente información:
```
#!properties
 ip=Ip donde se encuentra levantado el registry.
 puerto=Puerto en el cual se encuentra levantado el registry.
 sleepTime=Tiempo  en mili-segundos que la caja va a estar verificando su cola.
 cajaTime=Tiempo en mili-segundos que se demora en procesar un mercado. Este valor sera multiplicado por un numero aleatorio.
```
* Ejecute el archivo **supermercados-caja.bat** el cual ejecutara el supermercados-caja. Por defecto la caja se encuentra inactiva.

![supermercados-caja.png](https://bitbucket.org/repo/zngL6X/images/3949236850-supermercados-caja.png)

###supermercados-mercados:

**Instrucciones**

* Edite el archivo **mercados.properties** con la siguiente información:
```
#!properties
 ip=Ip donde se encuentra levantado el registry.
 puerto=Puerto en el cual se encuentra levantado el registry.
 hilos=Número de hilos que van a estar generando mercados.
 iteraciones=Número de iteraciones que va a tener cada hilo.
 sleepTime=Tiempo en mili-segundos para esperar entre cada iteración.
```
* Ejecute el archivo **supermercados-mercados.bat** el cual ejecutara el supermercados-mercados. Este proceso empezara la simulación del supermercado.

![supermercados-mercados.png](https://bitbucket.org/repo/zngL6X/images/2197517364-supermercados-mercados.png)