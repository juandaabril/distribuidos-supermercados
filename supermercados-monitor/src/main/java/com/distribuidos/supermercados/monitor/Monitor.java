package com.distribuidos.supermercados.monitor;

import java.rmi.RemoteException;
import java.util.List;

import com.distribuidos.supermercados.commun.interfaces.Caja;

/**
 * Clase Monitor. Clase encargada de verificar el estado de las cajas.
 * Dependiendo de unos valores puede activar o inactivar cajas.
 * 
 * @author Paola
 * @author Juan David
 */
public class Monitor extends Thread {

	private List<Caja> listaCajaInactiva;
	private List<Caja> listaCajaActiva;
	private long sleepTime;
	private int mercadosParaActivar;
	private int mercadosParaInactivar;

	public Monitor(List<Caja> listaCajaInactiva, List<Caja> listaCajaActiva,
			long sleepTime, int mercadosParaActivar, int mercadosParaInactivar) {
		this.listaCajaInactiva = listaCajaInactiva;
		this.listaCajaActiva = listaCajaActiva;
		this.sleepTime = sleepTime;
		this.mercadosParaActivar = mercadosParaActivar;
		this.mercadosParaInactivar = mercadosParaInactivar;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		while (true) {
			try {
				/*******************************************************************************
				 * Si no hay cajas activas y hay cajas suscritas se debe activar
				 * por lo menos 1
				 *******************************************************************************/
				if (listaCajaActiva.isEmpty() && listaCajaInactiva.size() > 0) {
					Caja caja = listaCajaInactiva.get(0);
					listaCajaInactiva.remove(caja);
					listaCajaActiva.add(caja);
					caja.setActivo(true);
				}

				/*******************************************************************************
				 * Se verifica la cola de cada uno de las cajas, si hay mas de 5
				 * mercados en cola y hay cajas disponibles para su activacion
				 * se activan
				 *******************************************************************************/
				int cantidadMercados = 0;
				for (Caja caja : listaCajaActiva) {
					cantidadMercados += caja.getCantidadElementosCola();
				}

				if (!listaCajaActiva.isEmpty()) {
					cantidadMercados = cantidadMercados	/ listaCajaActiva.size();
				} else {
					cantidadMercados = 0;
				}

				if (cantidadMercados > mercadosParaActivar) {
					activarCaja();
				} else if (cantidadMercados < mercadosParaInactivar	&& listaCajaActiva.size() > 1) {
					inactivaCaja();
				}

				Thread.sleep(sleepTime);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Método para activar la caja
	 * @throws RemoteException
	 */
	private void activarCaja() throws RemoteException {
		if (listaCajaInactiva.size() > 0) {
			Caja caja = listaCajaInactiva.get(0);
			listaCajaInactiva.remove(caja);
			listaCajaActiva.add(caja);
			caja.setActivo(true);
		}
	}

	/**
	 * Método para inactivar la caja
	 * @throws RemoteException
	 */
	private void inactivaCaja() throws RemoteException {
		if (listaCajaActiva.size() > 0) {
			Caja caja = listaCajaActiva.get(0);
			listaCajaActiva.remove(caja);
			listaCajaInactiva.add(caja);
			caja.setActivo(false);
		}
	}

}
