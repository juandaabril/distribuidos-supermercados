package com.distribuidos.supermercados.monitor;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.distribuidos.supermercados.commun.interfaces.Caja;
import com.distribuidos.supermercados.commun.interfaces.MonitorCaja;

/**
 * Clase MonitorCajaImpl. Clase que contiene la lógica necesaria para implementar la
 * interface MonitorCaja.
 * 
 * @author Paola
 * @author Juan David
 */
public class MonitorCajaImpl extends UnicastRemoteObject implements MonitorCaja{
	
	private static final long serialVersionUID = -2703462574597158826L;
	
	private int secuenciaCaja = 0;
	private List<Caja> listaCajaInactiva;
	private List<Caja> listaCajaActiva;

	protected MonitorCajaImpl(long sleepTime, int mercadosParaActivar, int mercadosParaInactivar) throws RemoteException {
		super();
		this.listaCajaInactiva = new ArrayList<Caja>();
		this.listaCajaActiva = new ArrayList<Caja>();
		
		/**************************************************************
		 * Se inicializa el HILO para monitorear el estado de las cajas
		 *************************************************************/
		Monitor monitor = new Monitor(listaCajaInactiva, listaCajaActiva, sleepTime, mercadosParaActivar, mercadosParaInactivar);
		monitor.start();
	}

	public synchronized void addCaja(Caja caja) throws RemoteException {
		try {
			String nombre = "Caja" + secuenciaCaja;
			caja.setNombre(nombre);
			listaCajaInactiva.add(caja);
			System.out.println("(Monitor)(addCaja): " + nombre);
			secuenciaCaja++;
		} catch (RemoteException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public synchronized Caja ubicarMercado() throws RemoteException {	
		if(listaCajaActiva.isEmpty()) {
			return null;
		}
		Collections.sort(listaCajaActiva, new Comparator<Caja>() {
			@Override
			public int compare(Caja o1, Caja o2) {
				try {
					if(o1== null || o2==null){
						return 1;
					}
					return o1.getCantidadElementosCola() - o2.getCantidadElementosCola() ;
				} catch (RemoteException e) {
					e.printStackTrace();
					return 1;
				}
			}
		});
		Caja caja = listaCajaActiva.get(0);
		System.out.println("(Monitor)(ubicarMercado):" + caja.getNombre());
		return caja;
	}

}
