package com.distribuidos.supermercados.monitor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Properties;

import com.distribuidos.supermercados.commun.interfaces.MonitorCaja;
import com.distribuidos.supermercados.commun.interfaces.SuperMercadoRegistry;
import com.distribuidos.supermercados.commun.util.Utils;

/**
 * Clase Main. Clase principal encargada de obtener los datos de las propiedades
 * y registrar el monitor en el registry
 * 
 * @author Paola
 * @author Juan David
 */
public class Main {
	
	public static void main(String args[]) throws FileNotFoundException,
			IOException, NotBoundException {

		System.out.println("Proyecto - SUPERMERCADOS - Sistemas Distribuidos - Monitor");

		/*************************************************************
		 * Se obtiene los datos de conexion del archivo de propiedades
		 *************************************************************/
		Properties prop = new Properties();
		prop.load(new FileInputStream("monitor.properties"));
		String ipRegistry = prop.getProperty("ip");
		int puertoRegistry = Integer.valueOf(prop.getProperty("puerto"));
		long sleepTime  = Long.valueOf(prop.getProperty("sleepTime"));
		int mercadosParaActivar  = Integer.valueOf(prop.getProperty("mercadosParaActivar"));
		int mercadosParaInactivar  = Integer.valueOf(prop.getProperty("mercadosParaInactivar"));
		String host = prop.getProperty("hostname");
		
		/***************************************************
		 * Se realizan set a la maquina virtual
		 **************************************************/
		Utils.setCodeBase(SuperMercadoRegistry.class);
		Utils.setHostName(host);
		
		/*****************************
		 * Se crea el objeto monitor
		 *****************************/
		MonitorCaja monitor = new MonitorCajaImpl(sleepTime, mercadosParaActivar, mercadosParaInactivar);
		
		/*************************************************************
		 * Se registra el monitor utilizando el supermercado registry
		 *************************************************************/
		final Registry registry = LocateRegistry.getRegistry(ipRegistry, puertoRegistry);
		final SuperMercadoRegistry superMercadoRegistry = (SuperMercadoRegistry)registry.lookup(SuperMercadoRegistry.RMI_NAME);
		superMercadoRegistry.registrar(monitor);
		
		
		System.out.println("Monitor Registrado con exito.  Presione enter para terminar");

		System.in.read();

	}

}
