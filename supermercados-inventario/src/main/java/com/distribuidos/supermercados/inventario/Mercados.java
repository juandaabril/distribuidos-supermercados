package com.distribuidos.supermercados.inventario;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.Random;

import com.distribuidos.supermercados.commun.model.Item;
import com.distribuidos.supermercados.commun.model.Mercado;

/**
 * Clase Mercados. Clase de utilidad para generar mercados segun valores
 * especificados en el archivo de propiedades.
 * 
 * @author Paola
 * @author Juan David
 */
public class Mercados {

	public static Queue<Mercado> colaMercadosDisponibles = new LinkedList<Mercado>();
	private static String codigos[] = { "COCA-COLA", "ARROZ", "CAFE", "SAL",
			"AZUCAR", "TOMATE", "PAPA", "YUCA", "MANTEQUILLA", "LECHE",
			"CEBOLLA", "AREPA", "HUEVO", "PANELA", "AJO", "FRIJOL", "LENTEJAS" };
	private static Map<String, Double> valores = new HashMap<String, Double>();

	static {

		/**************************************************************
		 * Items ofrecidos en el super-mercado con su respectivo valor
		 ************************************************************/
		valores.put("COCA-COLA", 3000.0);
		valores.put("ARROZ", 1300.0);
		valores.put("CAFE", 5000.0);
		valores.put("SAL", 500.0);
		valores.put("AZUCAR", 1000.0);
		valores.put("TOMATE", 100.0);
		valores.put("PAPA", 100.0);
		valores.put("YUCA", 500.0);
		valores.put("MANTEQUILLA", 3000.0);
		valores.put("LECHE", 2200.0);
		valores.put("CEBOLLA", 300.0);
		valores.put("AREPA", 1000.0);
		valores.put("HUEVO", 300.0);
		valores.put("PANELA", 1000.0);
		valores.put("AJO", 500.0);
		valores.put("FRIJOL", 1000.0);
		valores.put("LENTEJAS", 600.0);

		/*************************************************************
		 * Se obtiene los datos del archivo de propiedades
		 *************************************************************/
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("inventario.properties"));
		} catch (Exception ex) {
		}

		/**************************************************************
		 * Se obtienen las variables para el calculo de los mercados
		 ************************************************************/
		int mercadosAGenerar = Integer.valueOf(prop.getProperty("mercadosAGenerar"));
		int cantidadMaxItemPorMercado = Integer.valueOf(prop.getProperty("cantidadMaxItemPorMercado"));
		int cantidadMaxCompraItem = Integer.valueOf(prop.getProperty("cantidadMaxCompraItem"));
		int codigoItem = 0;
		int cantidadComprada = 0;
		Random rand = new Random();
		Mercado mercado = null;
		Item item = null;

		for (int i = 0; i < mercadosAGenerar; i++) {
			mercado = new Mercado();
			mercado.setNombre("Mercado" + i);
			mercado.setLista(new ArrayList<Item>());
			int cantidadItem = rand.nextInt(cantidadMaxItemPorMercado) + 1;
			for (int j = 0; j < cantidadItem; j++) {
				codigoItem = rand.nextInt(codigos.length);
				cantidadComprada = rand.nextInt(cantidadMaxCompraItem) + 1;
				item = new Item();
				item.setCodigo(codigos[codigoItem]);
				item.setCantidad(cantidadComprada);
				item.setValor(valores.get(codigos[codigoItem]));
				mercado.getLista().add(item);
			}

			System.out.println("Mercado Generado:" + mercado + "-Lista:" + mercado.getLista());

			colaMercadosDisponibles.add(mercado);
		}

	}

}
