package com.distribuidos.supermercados.inventario;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.distribuidos.supermercados.commun.interfaces.Inventario;
import com.distribuidos.supermercados.commun.model.Mercado;

/**
 * Clase InventarioImpl. Clase que contiene la lógica necesaria para implementar
 * la interface Inventario.
 * 
 * @author Paola
 * @author Juan David
 */
public class InventarioImpl extends UnicastRemoteObject implements Inventario {

	public InventarioImpl() throws RemoteException {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.distribuidos.supermercados.commun.interfaces.Inventario#generarMercado
	 * ()
	 */
	public synchronized Mercado generarMercado() throws RemoteException {
		Mercado mercado = Mercados.colaMercadosDisponibles.poll();
		System.out.println("Se genero:" + mercado);
		return mercado;
	}

}
